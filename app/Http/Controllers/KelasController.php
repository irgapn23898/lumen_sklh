<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KelasController extends Controller
{
    public function create(Request $request)
    {
      $validation = Validator::make($request->all(),[
        'id' => 'required|max:10',
        'nama_kelas' => 'required|string',
        'jurusan' => 'required|string',
      ]);

      if ($validation->fails()){
        $error = $validation->errors();
        return[
          'status' => 'Error',
          'message' => $error,
          'result' => null
        ];
      }
      $result = \App\Kelas::create($request->all());
      if($result){
        return[
          'status' => 'Success',
          'message' => 'Data berhasil ditambah',
          'result' => $result
        ];
      }else {
        return[
          'status' => 'Error',
          'message' => 'Data gagal ditambah',
          'result' => null
        ];
      }
    }

    public function read(Request $request)
    {
      $result = \App\Kelas::all();
      return[
        'status' => 'Success',
        'message' => '',
        'result' => $result
      ];
    }

    public function update(Request $request, $id)
    {
      $validation = Validator::make($Request->all(),[
        'id' => 'required|max:10',
        'nama_kelas' => 'required|string',
        'jurusan' => 'required|string',
      ]);

      if ($validation->fails()){
        $error = $validation->errors();
        return[
          'status' => 'Error',
          'message' => $error,
          'result' => null
        ];
    }

    $kelas = \App\Kelas::find($id);
    if (empty($kelas)){
      return[
        'status' => 'Error',
        'message' => 'Data tidak ditemukan',
        'result' => null
      ];
    }

    $result = $kelas->update($request->all());
    if($result){
      return[
        'status' => 'Success',
        'message' => 'Data berhasil diubah',
        'result' => $result
      ];
    }else {
      return[
        'status' => 'Error',
        'message' => 'Data gagal diubah',
        'result' => null
      ];
    }
  }

  public function delete(Request $request, $id)
  {
    $kelas = \App\Kelas::find($id);
    if (empty($kelas)){
      return[
        'status' => 'Error',
        'message' => 'Data tidak ditemukan',
        'result' => null
      ];
    }

    $result = $kelas->delete($id);
    if($result){
      return[
        'status' => 'Success',
        'message' => 'Data berhasil dihapus',
        'result' => $result
      ];
    }else {
      return[
        'status' => 'Error',
        'message' => 'Data gagal dihapus',
        'result' => null
      ];
    }
  }
}
